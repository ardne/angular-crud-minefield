package me.pjw.crud.db;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import me.pjw.crud.domain.Gender;
import me.pjw.crud.domain.User;

import org.springframework.stereotype.Repository;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

@Repository
public class UserRepository {

	private List<User> users;

	private User createUser(Long id, String login, String password, String firstname, String lastname, String email, Gender gender, Integer age, Boolean active, String note) {
		User user = new User();
		user.setId(id);
		user.setLogin(login);
		user.setPassword(password);
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setEmail(email);
		user.setGender(gender);
		user.setAge(age);
		user.setActive(active);
		user.setNote(note);
		return user;
	}

	private Long generateId() {
		Ordering<User> o = new Ordering<User>() {
			@Override
			public int compare(User left, User right) {
				return Longs.compare(left.getId(), right.getId());
			}
		};
		User u = o.max(users);
		return u.getId() + 1;
	}

	@PostConstruct
	public void init() {
		users = new ArrayList<User>();
		// id, login, password, firstname, lastname, e-mail, gender, age, active, note
		insert(createUser(1L, "joanna", "joanna", "Joanna", "Rojko", "yoanna@google.com", Gender.FEMALE, 28, true, "Boss"));
		insert(createUser(2L, "pawel", "pawel", "Paweł", "Górski", "pablo@pizzahut.pl", Gender.MALE, 27, true, null));
		insert(createUser(3L, "tomek", "tomek", "Tomasz", "Szymański", "tom@onet.pl", Gender.MALE, 30, false, null));
		insert(createUser(4L, "eliza", "eliza", "Eliza", "Rynkiewicz", "ela@milk.pl", Gender.FEMALE, 23, true, "Los Angeles Miss"));
		insert(createUser(5L, "zenek", "zenek", "Zenek", "Mistrzunio", "zenek@zenek.com", Gender.MALE, 35, true, "CEO"));
		insert(createUser(7L, "lukasz", "lukasz", "Łukasz", "Nowak", "lucky@wp.pl", Gender.MALE, 30, false, null));
		insert(createUser(8L, "gosia", "gosia", "Gosia", "Adamska", "gosia123@gmail.com", Gender.FEMALE, 28, true, null));
		insert(createUser(9L, "paulina", "paulina", "Paulina", "Kowalska", "paulina@tlen.pl", Gender.FEMALE, 23, true, "Temporary account"));
		insert(createUser(10L, "ala", "ala", "Alicja", "Olejniczak", "alicja@gmail.com", Gender.MALE, 31, false, "Test account"));
	}

	public void insert(User user) {
		if(user == null) return;
		if(user.getId() == null) {
			user.setId(generateId());
		}
		users.add(user);
	}

	public void update(User user) {
		if(user == null) return;
		User u = getUserById(user.getId());
		u.setFirstname(user.getFirstname());
		u.setLastname(user.getLastname());
		u.setLogin(user.getLogin());
		u.setEmail(user.getEmail());
		u.setActive(user.getActive());
		u.setNote(user.getNote());
//		u.setPassword(user.getPassword());
	}

	public void delete(Long id) {
		if(id == null) return;
		Iterables.removeIf(users, new Predicate<User>() {
			public boolean apply(User u) {
				return u.getId() == id;
			}
		});
	}

	public User getUserById(Long id) {
		if(id == null) return null;
		return Iterables.find(users, new Predicate<User>() {
			public boolean apply(User u) {
				return u.getId() == id;
			}
		});
	}

	public List<User> getUsers() {
		return users;
	}
}
