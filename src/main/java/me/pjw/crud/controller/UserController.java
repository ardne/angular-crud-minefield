package me.pjw.crud.controller;

import java.util.List;

import me.pjw.crud.domain.User;
import me.pjw.crud.domain.Users;
import me.pjw.crud.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public List<User> getUsers() {
		return userService.getUsers();
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public void createUser(@RequestBody User user) {
		userService.createUser(user);
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public User getUser(@PathVariable Long id) {
		return userService.getUserById(id);
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
	public void updateUser(@PathVariable Long id, @RequestBody User user) {
		user.setId(id);
		userService.updateUser(user);
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable Long id) {
		userService.deleteUser(id);
	}

	@RequestMapping(value = "/check/login/{login}", method = RequestMethod.GET)
	public boolean checkLoginAvailability(@PathVariable String login) {
		return userService.checkAvailability(Users.hasLogin(login));
	}

	@RequestMapping(value = "/check/email/{email:.+}", method = RequestMethod.GET)
	public boolean checkEmailAvailability(@PathVariable String email) {
		return userService.checkAvailability(Users.hasEmail(email));
	}
}
