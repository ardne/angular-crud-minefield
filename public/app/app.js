var app = angular.module('crud', ['ngRoute', 'ngResource', 'ngMessages']);

app.config(function ($routeProvider) {

    $routeProvider
        .when('/', {redirectTo: '/user/list'})
        .when('/user/list', {controller: 'UserListController', templateUrl: 'app/partials/user-list.html'})
        .when('/user/add', {controller: 'UserDetailCreationController', templateUrl: 'app/partials/user-add.html'})
        .when('/user/edit/:user_id', {controller: 'UserDetailEditController', templateUrl: 'app/partials/user-edit.html'})
        .when('/404_page', {controller: 'Controller404', templateUrl: 'app/partials/404_page_partial.html'})
        .otherwise({redirectTo: '/404_page'});
});