(function () {

	var compareTo = function () {
		return {
			require: 'ngModel',
			scope: {
				passwd: '=compareTo'
			},
			link: function(scope, element, attributes, ngModel) {

				ngModel.$validators.compareTo = function(modelValue) {
					return modelValue == scope.passwd;
				};

				scope.$watch('passwd', function(modelValue) {
					ngModel.$validate();
				});
			}
		};
	};

	app.directive('compareTo', compareTo);

	var unique = function ($http) {
		return {
			require: 'ngModel',
			link: function (scope, element, attrs, ngModel) {
				element.bind('blur', function () {
					$http.get('/check/' + attrs.unique + '/' + ngModel.$modelValue).success(function (response) {
						if(response == false) {
							ngModel.$setValidity('unique', false);
						} else {
							ngModel.$setValidity('unique', true);
						}
					});
				});
			}
		};
	};

	app.directive('unique', unique);

}());