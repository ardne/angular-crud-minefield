(function () {

	app.factory('UserService', function UserService($resource) {
		return $resource('/users/:id', {}, {
			update: {
				method: 'PUT',
				params: {id: '@id'}
			}
		});
    });

	app.factory('UserAvailabilityService', function UserAvailabilityService($resource) {
		return $resource('/check/:type/:data');
	});

}());